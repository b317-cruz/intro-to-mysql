/*
	NOTE: To import, unselect Enable foreign key checks during the Import process of the classic_models.sql file.
	
	classic_models (database name)

	customers: customerNumber (PK), customerName, contactLastName, contactFirstName, phone, addressLine1, addressLine2, city, state, postalCode, country, salesRepEmployeeNumber (FK employees.employeeNumber), creditLimit
	
	employees: employeeNumber (PK), lastName, firstName, extension, email, officeCode (FK offices.officeCode), reportsTo (FK employees.employeeNumber), jobTitle
	
	offices: officeCode (PK), city, phone, addressLine1, addressLine2, state, country, postalCode, territory
	
	orderdetails: orderNumber (PK/FK orders.orderNumber), productCode (PK/FK products.productCode), quantityOrdered, priceEach, orderLineNumber
	
	orders: orderNumber (PK), orderDate, requiredDate, shippedDate, status, comments, customerNumber (FK customers.customerNumber)
	
	payments: customerNumber (PK/FK customers.customerNumber), checkNumber (PK), paymentDate, amount
	
	productlines: productLine (PK), textDescription, htmlDescription, image
	
	products: productCode (PK), productName, productLine (FK productLines.productLine), productScale, productVendor, productDescription, quantityInStock, buyPrice, MSRP
*/

/* TASK #1 - Return the customerName of the customers who are from the Philippines */
SELECT customerName
FROM customers
WHERE country = 'Philippines';

/* TASK #2 - Return the contactLastName and the contactFirstName of customers with name "La Rochelle Gifts" */
SELECT contactLastName, contactFirstName
FROM customers
WHERE customerName = 'La Rochelle Gifts';

/* TASK #3 - Return the product name and the MSRP of the product named "The Titanic" */
SELECT productName, MSRP
FROM products
WHERE productName = 'The Titanic';

/* TASK #4 - Return the first and last name of the employee whose email is "jfirrelli@classicmodelcars.com" */
SELECT firstName, lastName
FROM employees
WHERE email = 'jfirrelli@classicmodelcars.com';

/* TASK #5 - Return the names of customers who have no registered state */
SELECT customerName
FROM customers
WHERE state IS NULL OR state = '';

/* TASK #6 - Retun the first name, last name, and email of the employee whose last name is 'Patterson' and first name is 'Steve' */
SELECT firstName, lastName, email
FROM employees
WHERE lastName = 'Patterson' AND firstName = 'Steve';

/* TASK #7 - Return customer name, country, and credit limit of customers whose countries are NOT USA and whose credit limits are greater than 3000 */
SELECT customerName, country, creditLimit
FROM customers
WHERE country NOT IN ('USA') AND creditLimit > 3000;

/* TASK #8 - Return the customer number of orders whose comments contain the string 'DHL' */
SELECT customerNumber
FROM orders
WHERE comments LIKE '%DHL%';

/* TASK #9 - Return the product lines whose text description mentions the phrase 'state of the art' */
SELECT productLine
FROM productlines
WHERE textDescription LIKE '%state of the art%';

/* TASK #10 - Return the countries of customers without duplication */
SELECT DISTINCT country
FROM customers;

/* TASK #11 - Return the statuses of orders without duplication */
SELECT DISTINCT status
FROM orders;

/* TASK #12 - Return the customer names and countries of customers whose country is USA, France, or Canada */
SELECT customerName, country
FROM customers
WHERE country IN ('USA', 'France', 'Canada');

/* TASK #13 - Return the first name, last name, and office's city of employees whose offices are in Tokyo */
SELECT employees.firstName, employees.lastName, offices.city
FROM employees
JOIN offices ON employees.officeCode = offices.officeCode
WHERE offices.city = 'Tokyo';

/* TASK #14 - Return the customer names of customers who were served by the employee named "Leslie Thompson" */
SELECT customers.customerName
FROM customers
JOIN employees ON customers.salesRepEmployeeNumber = employees.employeeNumber
WHERE employees.firstName = 'Leslie' AND employees.lastName = 'Thompson';

/* TASK #15 - Return the product names and customer name of products ordered by "Baane Mini Imports" */
SELECT products.productName, customers.customerName
FROM products
JOIN orderdetails ON products.productCode = orderdetails.productCode
JOIN orders ON orderdetails.orderNumber = orders.orderNumber
JOIN customers ON orders.customerNumber = customers.customerNumber
WHERE customers.customerName = 'Baane Mini Imports';

/* TASK #16 - Return the employees' first names, employees' last names, customers' names, and offices' countries of transactions whose customers and offices are located in the same country */
SELECT employees.firstName, employees.lastName, customers.customerName, offices.country AS office_country
FROM employees
JOIN customers ON employees.employeeNumber = customers.salesRepEmployeeNumber
JOIN offices ON employees.officeCode = offices.officeCode
WHERE customers.country = offices.country;

/* TASK #17 - Return the product name and quantity in stock of products that belong to the product line "planes" with stock quantities less than 1000 */
SELECT products.productName, products.quantityInStock
FROM products
JOIN productlines ON products.productLine = productlines.productLine
WHERE productlines.productLine = 'Planes' AND products.quantityInStock < 1000;

/* TASK #18 - Return the customer's name with a phone number containing "+81" */
SELECT customerName
FROM customers
WHERE phone LIKE '%+81%';