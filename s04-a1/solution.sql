/*
	music_db
	albums: id, album_title, date_released, artist_id (id from artists)
	artists: id, name
	playlists: id, user_id (id from users), datetime_created
	playlists_songs: id, playlist_id (id from playlists), song_id (id from songs)
	songs: id, song_name, length, genre, album_id (id from albums)
	users: id, username, password, full_name, contact_number, email, address
*/

/* TASK A - Find all artists that has letter 'd' in its name */
SELECT *
FROM artists
WHERE name LIKE '%d%';

/* TASK B - Find all songs that has a length of less than 3:50 / 2:30 */
SELECT *
FROM songs
WHERE length < '00:02:30'; -- Just followed the Sample Output

/* TASK C - Join the 'albums' and 'songs' tables. (Only show the album name, song name, and song length) */
SELECT albums.album_title, songs.song_name, songs.length
FROM albums
JOIN songs ON albums.id = songs.album_id;
-- Different output from Sample Output because of differing songs and albums records (e.g., no TWICE or BLACKPINK related records are inserted on the database prior)

/* TASK D - Join the 'artists' and 'albums' tables. (Find all albums that has the letter 'a' in its name). The columns that will show on each table are: artists - id, name AND albums - id, album_title, date_released, artists_id */
SELECT artists.id, artists.name, albums.id, albums.album_title, albums.date_released, albums.artist_id
FROM artists
JOIN albums ON artists.id = albums.artist_id
WHERE albums.album_title LIKE '%a%';
-- Different output from Sample Output since row number 2 on Sample output has no letter 'a' on it and provided database has a different record (eg., no BLACKPINK-related record is on the database)

/* TASK E - Sort the albums in Z-A order. (Show only the first 4 records) */
SELECT *
FROM albums
ORDER BY album_title DESC
LIMIT 4;
-- Different output from Sample Output (e.g., no BLACKPINK-related record is on the database)

/* TASK F - Join the 'albums' and 'songs' tables. (Sort albums from Z-A). Show the follow tables and fields: albums - id, album_title, date_released, artist_id AND songs - id, song_name, length, genre, album_id */
SELECT albums.id, albums.album_title, albums.date_released, albums.artist_id, songs.id, songs.song_name, songs.length, songs.genre, songs.album_id
FROM albums
JOIN songs ON albums.id = songs.album_id
ORDER BY albums.album_title DESC;